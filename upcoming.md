## **[PUBLIC] search/job**

	- params
		- title
		- location
		- preferred pay-rate
		if: signed in
		- match quick cv (checkbox)
		- save search

	- return
		- job[] w/
			- title
				- on job search results page, which title to show? diff agengies might post with diff desc and title
			- dec
			- location
			- rate range
				- range showed based on minimum and max posted by each agency?
			- postedBy[]

## **[PUBLIC] search/job/:id/:recruiterId**
	return
		- title
		- desc
		- responsibilities
		- required skills
		- pay rate
		- location
		- agency

## **[SEEKER] job/apply/:id/:recruiterId**
    - apply to the job

## **[SEEKER] /job/saved**

	- search query for saved

## **[SEEKER] job/save/:id/:recruiterId**
    - save the job

## **[SEEKER] /job/applied**

	- search query for applied
		- has application phases

## **[SEEKER] cv/ (upsert)**

	- params
		- skills[] (min 10, max 15, isnt min 10 too high?)
			- name
			- years
		- current org
		- current desigation
		- incorperated
			- cant these 3 go in the profile?
			- or allow these to be edited from here and also add the incorperated field on the profile page