const router = require("express").Router();
const roleCheck = require("../middleware/role-check");
const scrubBodyProperties = require("../middleware/scrubber");
const passport = require("passport");
const Job = require("../models/job").Job;
const JobGroup = require("../models/job-group").JobGroup;

// router.use(passport.authenticate("jwt", { session: false }))
// router.use(roleCheck('recruiter'))

/**
 * post a job as a recruiter
 */
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  roleCheck("recruiter"),
  (req, res) => {
    Job.findOne({ uid: req.user._id + "@" + req.body.jobId }).then(d => {
      if (!d) {
        const job = new Job({
          uid: req.user._id + "@" + req.body.jobId,
          jobId: req.body.jobId,
          title: req.body.title,
          description: req.body.description,
          employer: req.body.employer,
          startDate: req.body.startDate,
          endDate: req.body.endDate,
          minRate: req.body.minRate,
          maxRate: req.body.maxRate,
          skills: req.body.skills,
          applicationCloseDate: req.body.applicationCloseDate,
          postedBy: req.user._id,
          createdAt: new Date(),
          updatedAt: new Date()
        });
        if (req.body.confidential) {
          job.isConfidential = true;
        }
        job.save().then(j => {
          const query = { jobId: j.jobId };
          const update = { $addToSet: { jobs: j._id } };
          const options = { upsert: true, new: true };

          JobGroup.findOneAndUpdate(query, update, options)
            .then(group => {
              job.groupId = group._id;
              job.save().then(_ => {
                res.send(j.toObject());
              });
            })
            .catch(err =>
              res.status(500).send({
                error: true,
                message: "error creating/updating jobgroup"
              })
            );
        });
      } else {
        res.status(500).send({ exists: true, job: d.toObject() });
      }
    });
  }
);

/**
 * view own submitted jobs as a recruiter
 */
router.get(
  "/my-postings",
  passport.authenticate("jwt", { session: false }),
  roleCheck("recruiter"),
  (req, res) => {
    // res.send({query: req.query});
    const sort = req.query.sort === "asc" ? 1 : -1;
    let limit = req.query.size ? req.query.size : 10;
    limit = limit < 0 ? 10 : limit;
    let skip = req.query.page ? (req.query.page - 1) * limit : 0;
    skip = skip < 0 ? 0 : skip;

    Job.find({ postedBy: req.user._id }, null, {
      sort: { createdAt: sort },
      skip: Number(skip),
      limit: Number(limit)
    })
      .then(d => {
        const jobs = d.map(job => {
          const j = job.toObject();
          delete j.postedBy;
          return j;
        });
        res.send(jobs);
      })
      .catch(err => {
        res.status(500).send({ error: err });
      });
  }
);

/**
 * edit your own job posting as a recruiter
 */
router.put(
  "/:jobId",
  passport.authenticate("jwt", { session: false }),
  roleCheck("recruiter"),
  scrubBodyProperties(["_id", "uid"]),
  (req, res) => {
    const query = { uid: req.user._id + "@" + req.params.jobId };
    const updateFields = { ...req.body, updatedAt: new Date() };
    const options = { new: true };
    Job.findOneAndUpdate(query, updateFields, options)
      .then(job => {
        res.send(job.toObject());
      })
      .catch(err => {
        res.status(500).send({
          error: true,
          message: err
            ? err.message
            : `job with Id ${req.params.id} doesn't exist`
        });
      });
  }
);

/**
 * apply for a job as a seeker
 */
router.post(
  "/apply/:jobId",
  passport.authenticate("jwt", { session: false }),
  roleCheck("seeker"),
  (req, res) => {
    const query = { _id: req.params.jobId };
    const updateFields = { $addToSet: { applicants: req.user._id } }
    const options = { new: false }
    Job.findOneAndUpdate(query, updateFields, options)
      .then(doc => {
        if (doc.applicants.indexOf(req.user._id) >= 0) {
          res.status(500).send({ error: true, message: 'you have already applied to this job' })
        } else {
          res.send({ success: true, message: 'applied successfully' })
        }
      })
      .catch(err => {
        res.status(500).send({
          error: true,
          message: err
            ? err.message
            : `error applying to job with Id ${req.params.id}.`
        });
      });
  }
);

/**
 * see jobs that a seeker has applied to
 */
router.get(
  "/my-applied",
  passport.authenticate("jwt", { session: false }),
  roleCheck("seeker"),
  (req, res) => {
    const sort = req.query.sort === "asc" ? 1 : -1;
    let limit = req.query.size ? req.query.size : 10;
    limit = limit < 0 ? 10 : limit;
    let skip = req.query.page ? (req.query.page - 1) * limit : 0;
    skip = skip < 0 ? 0 : skip;

    const query = { applicants: { $in: [req.user._id] } }

    Job.find(query, ['-applicants'], {
      sort: { createdAt: sort },
      skip: Number(skip),
      limit: Number(limit)
    })
      .populate({
        path: 'postedBy',
        model: 'User',
        select: ['profile.firstName', 'profile.lastName']
      })
      .then(d => {
        const cleanedJobs = d.map(j => {
          const job = j.toObject();
          return {
            ...job,
            postedBy: { ...job.postedBy.profile, id: j.postedBy._id }
          }
        })
        res.send(cleanedJobs);
      })
      .catch(err => {
        res.status(500).send({ error: err });
      });
  }
);


/**
 * delete own job as a recruiter
 */
router.delete(
  "/:jobId",
  passport.authenticate("jwt", { session: false }),
  roleCheck("recruiter"),
  (req, res) => {
    Job.findOneAndDelete(
      { uid: req.user._id + "@" + req.params.jobId },
      { rawResult: true }
    )
      .then(doc => {
        if (doc && doc.ok) {
          console.log(doc.value.id);

          const query = { jobId: req.params.jobId };
          const update = { $pull: { jobs: doc.value.id } };
          JobGroup.findOneAndUpdate(query, update, { new: true })
            .then(group => {
              console.log(group);

              res.send({ success: true });
            })
            .catch(err => {
              console.log(err);
              res.status(500).send({
                error: true,
                message: `job with Id ${
                  req.params.jobId
                  } has already been deleted or doesn't exist.`
              });
            });
        } else {
          res.status(500).send({
            error: true,
            message: `job with Id ${
              req.params.jobId
              } has already been deleted or doesn't exist.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          error: true,
          message: err
            ? err.message
            : `job with Id ${req.params.id} doesn't exist`
        });
      });
  }
);

module.exports = router;
