const router = require("express").Router();
const Job = require("../models/job").Job;
const JobGroup = require("../models/job-group").JobGroup;

/**
 * see details of a single job
 */
router.get('/job/:id', (req, res) => {
    Job.findOne({ _id: req.params.id })
        .populate('postedBy', ['profile.firstName', 'profile.lastName'])
        .then(d => {
            if (d) {
                const j = d.toObject();
                delete j.postedBy._id
                delete j.uid
                delete j._id
                res.send(j)
            } else {
                res.status(500).send({ error: true, message: 'job not found' })
            }
        })
        .catch(err => {
            res.status(500).send({ error: err })
        })
})

/**
 * get jobs in a job group
 */
router.get('/group/:id', (req, res) => {
    JobGroup.findOne({ _id: req.params.id })
        .populate({
            path: 'jobs',
            populate: {
                path: 'postedBy',
                model: 'User',
                select: ['+_id', 'profile.firstName', 'profile.lastName']
            }
        })
        .then(d => {
            const cleanedJobs = d.toObject().jobs.map((job, i) => {
                return {
                    ...job,
                    postedBy: { ...job.postedBy.profile, id: job.postedBy._id }
                }
            })
            if (d) {
                res.send(cleanedJobs)
            } else {
                res.status(500).send({ error: err, message: `no records with jobId ${req.params.id}` })
            }
        })
        .catch(err => {
            res.status(500).send({ error: err })
        })
})

/**
 * see all jobs posted by a recruiter
 */
router.get('/:recId/jobs', (req, res) => {
    Job.find({ postedBy: req.params.recId })
        .populate('postedBy', ['profile.firstName', 'profile.lastName'])
        .then(d => {
            const jobs = d.map(job => {
                const j = job.toObject();
                delete j.applicants
                delete j.postedBy._id
                delete j.uid
                delete j._id
                return j;
            });
            res.send(jobs)
        })
        .catch(err => {
            res.status(500).send({ error: err })
        })
})

module.exports = router;
