const router = require("express").Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");

const User = require("../models/user");
const Profile = require("../models/profile").Profile;

router.post("/register", (req, res) => {
  if (!req.body.email || !req.body.password || !req.body.firstName || !req.body.lastName || !req.body.role) {
    res.status(500).send({ success: false, message: "first name, last name, role, email, and password required." });
  } else {
    const user = new User({
      email: req.body.email,
      password: req.body.password,
      // firstName: req.body.firstName,
      // lastName: req.body.lastName,
      role: req.body.role,
    });

    const profile = new Profile({
      firstName: req.body.firstName,
      lastName: req.body.lastName
    })
    user.profile = profile

    user.save().then(u => {
      const cleanedUser = {
        role: u.role,
        email: u.email,
      }
      const token = jwt.sign(cleanedUser, process.env.SECRET_KEY, {
        expiresIn: 86400
      });
      res.send({ success: true, token: token, message: "user created." });
    }).catch(err => {
      return res.status(500).send({ success: false, message: "email already exists.", err: err });
    })
  }
});

router.post("/authenticate", (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (err) {
      throw err;
    }
    if (user) {
      user.comparePassword(req.body.password, (err, matched) => {
        if (!err && matched) {
          const cleanedUser = {
            role: user.role,
            email: user.email,
          }
          const token = jwt.sign(cleanedUser, process.env.SECRET_KEY, {
            expiresIn: 86400
          });
          res.send({ success: true, token: token });
        } else {
          res.status(500).send({ success: false, message: "password did not match" });
        }
      });
    }
  });
});

router.get(
  "/dashboard",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.send({ id: req.user._id, role: req.user.role });
  }
);

router.get('/profile', passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const profile = req.user.toObject().profile
    profile.email = req.user.email;
    delete profile._id
    res.send(profile);
  })

router.put(
  "/profile",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const user = req.user
    const keysToUpdate = Object.keys(req.body);
    keysToUpdate.forEach(k => {
      if (user.profile[k] || user.profile[k] === null) {
        user.profile[k] = req.body[k]
      } else if (k === 'email') {
        user[k] = req.body[k]
      }
    })
    user.save().then(d => {
      const profile = d.profile.toObject();
      profile.email = user.email;
      delete profile._id
      res.send(profile)
    })
      .catch(err => {
        res.status(500).send({ message: 'error setting properties: ' + Object.keys(err.errors.profile.errors) })
        // res.status(500).send({message: err.message})
      })
  }
);

module.exports = router;
