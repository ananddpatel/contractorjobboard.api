

module.exports = role => {
  return (req, res, next) => {
    // console.log(req.user);
    if (req.user.role === role) {
      next();
    } else {
      res.status(401).send({ error: true, message: 'unauthorized role' })
    }
  }
};