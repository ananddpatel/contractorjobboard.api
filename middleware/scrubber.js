

module.exports = properties => {
  return (req, res, next) => {
    properties.forEach(prop => {
      delete req.body[prop]
    });
    next();
  }
};
