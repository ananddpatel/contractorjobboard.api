const mongoose = require("mongoose");

const ProfileSchema = new mongoose.Schema({
  // _id: String,
  // email: {
  //   type: String,
  //   lowercase: true,
  //   unique: true,
  //   required: true
  // },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    default: null
  },
  currEmployer: {
    type: String,
    default: null
  },
  currContractStart: {
    type: Date,
    default: null
  },
  currContractEnd: {
    type: Date,
    default: null
  },
  availability: {
    type: String,
    enum: ["notAvailable", "immediately", "in2Weeks", "in1Month"],
    default: "immediately"
  },
  linkedInProfile: {
    type: String,
    default: null
  },
  dob: {
    type: Date,
    default: null
  },
  location: {
    type: String,
    default: null
  },
  preferredDesignation: {
    type: [
      {
        type: String,
        required: true
      }
    ],
    default: []
  },
  preferredIndustry: {
    type: [
      {
        type: String,
        required: true
      }
    ],
    default: []
  },
  preferredPayRate: {
    type: String,
    default: '0'
  },
  preferredLocation: {
    type: [
      {
        type: String,
        required: true
      }
    ],
    default: []
  }
});

const Profile = mongoose.model("Profile", ProfileSchema);

module.exports = {
  Profile: Profile,
  ProfileSchema: ProfileSchema
};
