const mongoose = require("mongoose");

const JobGroupSchema = new mongoose.Schema(
  {
    jobId: { type: String, unique: true, index: true },
    jobs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Job' }]
  },
  {
    toObject: {
      transform: function (doc, ret, game) {
        delete ret.__v;
        delete ret._id;
        // ret.id = doc._id
      }
    }
  }
);

const JobGroup = mongoose.model("JobGroup", JobGroupSchema);

module.exports = {
  JobGroup: JobGroup
};
