const mongoose = require("mongoose");
const User = require("../models/user");

const JobSchema = new mongoose.Schema(
  {
    uid: { type: String, unique: true },
    jobId: String,
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    employer: {
      type: String,
      required: true
    },
    isConfidential: {
      type: Boolean,
      default: false
    },
    startDate: {
      type: Date,
      required: true
    },
    endDate: {
      type: Date,
      required: true
    },
    minRate: {
      type: Number,
      required: true
    },
    maxRate: {
      type: Number,
      required: true
    },
    skills: {
      type: [
        {
          type: String
        }
      ],
      default: [],
      maxlength: 10
    },
    applicationCloseDate: Date,
    postedBy: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User',
    },
    applicants: {
      type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
      default: []
    },
    groupId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'JobGroup'
    },
    createdAt: {
      type: Date,
      required: true
    },
    updatedAt: {
      type: Date,
      required: true
    }
  },
  {
    toObject: {
      transform: function (doc, ret, game) {
        delete ret.__v;
        delete ret.uid;
        delete ret._id
        ret.id = doc._id
      }
    }
  }
);

const Job = mongoose.model("Job", JobSchema);

module.exports = {
  Job: Job
};
